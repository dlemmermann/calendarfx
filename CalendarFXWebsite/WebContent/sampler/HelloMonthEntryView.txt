/**
 * Copyright (C) 2015 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.demo.entries;

import java.time.LocalDate;

import com.calendarfx.model.Entry;
import com.calendarfx.view.EntryViewBase;
import com.calendarfx.view.MonthEntryView;

public class HelloMonthEntryView extends HelloEntryViewBase {

	public HelloMonthEntryView() {
		super();

		entry.setStartDate(LocalDate.now());
		entry.setEndDate(LocalDate.now().plusDays(5));
	}

	@Override
	protected EntryViewBase<?> createEntryView(Entry<?> entry) {
		MonthEntryView view = new MonthEntryView(entry);
		view.setPrefSize(400, 20);
		return view;
	}

	@Override
	public String getSampleName() {
		return "Month Entry View";
	}

	@Override
	public String getSampleDescription() {
		return "This view is used to display a single entry in a month view.";
	}

	public static void main(String[] args) {
		launch(args);
	}
}
