/**
 * Copyright (C) 2015 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.demo.popover;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import com.calendarfx.demo.CalendarFXSample;
import com.calendarfx.model.Calendar;
import com.calendarfx.model.Calendar.Style;
import com.calendarfx.view.CalendarSelector;

public class HelloCalendarSelector extends CalendarFXSample {

	private CalendarSelector view;

	@Override
	public String getSampleName() {
		return "Calendar Selector";
	}

	@Override
	public Node getPanel(Stage stage) {
		HBox box = new HBox();
		box.setAlignment(Pos.CENTER);
		box.setFillHeight(false);

		StackPane stackPane = new StackPane();
		stackPane
				.setStyle("-fx-background-color: white; -fx-border-color: gray; -fx-border-width: .25px; -fx-padding: 20px;");
		box.getChildren().add(stackPane);

		Calendar meetings = new Calendar("Meetings");
		Calendar training = new Calendar("Training");
		Calendar customers = new Calendar("Customers");
		Calendar holidays = new Calendar("Holidays");

		meetings.setStyle(Style.STYLE2);
		training.setStyle(Style.STYLE3);
		customers.setStyle(Style.STYLE4);
		holidays.setStyle(Style.STYLE5);

		view = new CalendarSelector();
		view.getCalendars().addAll(meetings, training, customers, holidays);
		view.setCalendar(meetings);

		Label label = new Label("Selected: " + view.getCalendar().getName());
		label.setMaxHeight(Double.MAX_VALUE);
		view.calendarProperty().addListener(it -> label.setText("Selected: " + view.getCalendar().getName()));

		HBox box2 = new HBox(20);
		box2.setFillHeight(true);
		box2.getChildren().addAll(view, label);

		stackPane.getChildren().add(box2);

		stackPane.setEffect(new Reflection());

		return box;
	}

	@Override
	public String getSampleDescription() {
		return "A view used to select a calendar from a list of calendars.";
	}

	public static void main(String[] args) {
		launch(args);
	}
}
