/**
 * Copyright (C) 2015 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.demo.util;

import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import com.calendarfx.demo.CalendarFXSample;
import com.calendarfx.model.Calendar;
import com.calendarfx.model.CalendarSource;
import com.calendarfx.model.Calendar.Style;
import com.calendarfx.view.SourceView;

public class HelloCalendarSourceView extends CalendarFXSample {

	private SourceView sourceView;

	private CalendarSource workCalendarSource;

	private CalendarSource familyCalendarSource;

	@Override
	public String getSampleName() {
		return "Calendar Source View";
	}

	@Override
	public Node getPanel(Stage stage) {
		HBox box = new HBox();
		box.setStyle("-fx-padding: 100px;");
		box.setAlignment(Pos.CENTER);
		box.setFillHeight(false);

		StackPane stackPane = new StackPane();
		stackPane
				.setStyle("-fx-background-color: white; -fx-border-color: gray; -fx-border-width: .25px; -fx-padding: 50 20 50 20;");
		box.getChildren().add(stackPane);

		sourceView = new SourceView();
		stackPane.getChildren().add(sourceView);

		stackPane.setEffect(new Reflection());

		Calendar meetings = new Calendar("Meetings");
		Calendar training = new Calendar("Training");
		Calendar customers = new Calendar("Customers");
		Calendar holidays = new Calendar("Holidays");

		meetings.setStyle(Style.STYLE2);
		training.setStyle(Style.STYLE3);
		customers.setStyle(Style.STYLE4);
		holidays.setStyle(Style.STYLE5);

		workCalendarSource = new CalendarSource("Work");
		workCalendarSource.getCalendars().addAll(meetings, training, customers,
				holidays);

		Calendar birthdays = new Calendar("Birthdays");
		Calendar katja = new Calendar("Katja");
		Calendar dirk = new Calendar("Dirk");
		Calendar philip = new Calendar("Philip");
		Calendar jule = new Calendar("Jule");
		Calendar armin = new Calendar("Armin");

		familyCalendarSource = new CalendarSource("Family");
		familyCalendarSource.getCalendars().addAll(birthdays, katja, dirk,
				philip, jule, armin);

		sourceView.getCalendarSources().addAll(workCalendarSource,
				familyCalendarSource);

		return box;
	}

	@Override
	public Node getControlPanel() {
		VBox box = new VBox();
		box.setSpacing(5);
		box.setFillWidth(true);

		Button addWorkCalendar = new Button("Add Work Calendar");
		addWorkCalendar.setOnAction(evt -> addWorkCalendar());

		Button removeWorkCalendar = new Button("Remove Work Calendar");
		removeWorkCalendar.setOnAction(evt -> removeWorkCalendar());

		Button addWorkCalendarSource = new Button("Add Work Calendar Source");
		addWorkCalendarSource.setOnAction(evt -> addWorkCalendarSource());

		Button removeWorkCalendarSource = new Button(
				"Remove Work Calendar Source");
		removeWorkCalendarSource.setOnAction(evt -> removeWorkCalendarSource());

		box.getChildren().addAll(addWorkCalendar, removeWorkCalendar,
				addWorkCalendarSource, removeWorkCalendarSource);

		Button addFamilyCalendar = new Button("Add Family Calendar");
		addFamilyCalendar.setOnAction(evt -> addFamilyCalendar());

		Button removeFamilyCalendar = new Button("Remove Family Calendar");
		removeFamilyCalendar.setOnAction(evt -> removeFamilyCalendar());

		Button addFamilyCalendarSource = new Button(
				"Add Family Calendar Source");
		addFamilyCalendarSource.setOnAction(evt -> addFamilyCalendarSource());

		Button removeFamilyCalendarSource = new Button(
				"Remove Family Calendar Source");
		removeFamilyCalendarSource
				.setOnAction(evt -> removeFamilyCalendarSource());

		box.getChildren().addAll(new Separator(Orientation.HORIZONTAL),
				addFamilyCalendar, removeFamilyCalendar,
				addFamilyCalendarSource, removeFamilyCalendarSource);

		addWorkCalendar.setMaxWidth(Double.MAX_VALUE);
		removeWorkCalendar.setMaxWidth(Double.MAX_VALUE);
		addWorkCalendarSource.setMaxWidth(Double.MAX_VALUE);
		removeWorkCalendarSource.setMaxWidth(Double.MAX_VALUE);

		addFamilyCalendar.setMaxWidth(Double.MAX_VALUE);
		removeFamilyCalendar.setMaxWidth(Double.MAX_VALUE);
		addFamilyCalendarSource.setMaxWidth(Double.MAX_VALUE);
		removeFamilyCalendarSource.setMaxWidth(Double.MAX_VALUE);

		return box;
	}

	private int calendarCounter = 1;

	private void addWorkCalendar() {
		Calendar calendar = new Calendar("Work Calendar " + calendarCounter++);
		calendar.setStyle(Style.getStyle(calendarCounter));
		workCalendarSource.getCalendars().add(calendar);
	}

	private void removeWorkCalendar() {
		workCalendarSource.getCalendars().remove(
				workCalendarSource.getCalendars().size() - 1);
	}

	private void addWorkCalendarSource() {
		sourceView.getCalendarSources().add(workCalendarSource);
	}

	private void removeWorkCalendarSource() {
		sourceView.getCalendarSources().remove(workCalendarSource);
	}

	private void addFamilyCalendar() {
		Calendar calendar = new Calendar("Family Calendar " + calendarCounter++);
		calendar.setStyle(Style.getStyle(calendarCounter));
		familyCalendarSource.getCalendars().add(calendar);
	}

	private void addFamilyCalendarSource() {
		sourceView.getCalendarSources().add(familyCalendarSource);
	}

	private void removeFamilyCalendar() {
		familyCalendarSource.getCalendars().remove(
				familyCalendarSource.getCalendars().size() - 1);
	}

	private void removeFamilyCalendarSource() {
		sourceView.getCalendarSources().remove(familyCalendarSource);
	}

	@Override
	public String getSampleDescription() {
		return "Shows all calendar sources. Sources are used to group calendars together that all origin from the same "
				+ "source, e.g. Google calendar. Sources can be collapsed by clicking on their name.";
	}

	public static void main(String[] args) {
		launch(args);
	}
}
