/**
 * Copyright (C) 2015 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.demo.views;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.YearMonth;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import com.calendarfx.demo.CalendarPropertySheet;
import com.calendarfx.demo.CalendarFXSample;
import com.calendarfx.model.Calendar;
import com.calendarfx.model.CalendarSource;
import com.calendarfx.model.Entry;
import com.calendarfx.view.CalendarView;

public class HelloCalendarView extends CalendarFXSample {

	private CalendarView calendarView;

	@Override
	public String getSampleName() {
		return "Calendar View";
	}

	@Override
	public Node getPanel(Stage stage) {
		StackPane outerPane = new StackPane();
		outerPane.setStyle("-fx-padding: 20px;");

		StackPane stackPane = new StackPane();
		stackPane
				.setStyle("-fx-background-color: white; -fx-border-color: gray; -fx-border-width: .25px; -fx-padding: 20px;");
		outerPane.getChildren().add(stackPane);

		calendarView = new CalendarView();
		calendarView.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);

		CalendarSource calendarSource = new CalendarSource("My Calendars");
		calendarSource.getCalendars().add(new HelloCalendar());

		calendarView.getCalendarSources().add(calendarSource);
		StackPane.setAlignment(calendarView, Pos.CENTER);

		stackPane.getChildren().add(calendarView);

		return outerPane;
	}

	@Override
	public Node getControlPanel() {
		return new CalendarPropertySheet(calendarView.getPropertySheetItems());
	}

	@Override
	public String getSampleDescription() {
		return "The calendar view displays a single day, a week, a month, and a year.";
	}

	class HelloCalendar extends Calendar {

		public HelloCalendar() {
			for (Month month : Month.values()) {

				YearMonth yearMonth = YearMonth.of(LocalDate.now().getYear(), month);

				for (int i = 1; i < 28; i++) {

					LocalDate date = yearMonth.atDay(i);

					for (int j = 0; j < (int) (Math.random() * 7); j++) {
						Entry<?> entry = new Entry<>();
						entry.setStartDate(date);
						entry.setEndDate(date);

						entry.setTitle("Entry " + (j + 1));

						int hour = (int) (Math.random() * 23);
						int durationInHours = Math.min(24 - hour,
								(int) (Math.random() * 4));

						LocalTime startTime = LocalTime.of(hour, 0);
						LocalTime endTime = startTime
								.plusHours(durationInHours);

						entry.setStartTime(startTime);
						entry.setEndTime(endTime);

						if (Math.random() < .3) {
							entry.setFullDay(true);
						}

						addEntry(entry);
					}
				}
			}
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
