/**
 * Copyright (C) 2015 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.demo.views;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import com.calendarfx.demo.CalendarPropertySheet;
import com.calendarfx.demo.CalendarFXSample;
import com.calendarfx.view.WeekDayHeaderView;

public class HelloWeekDayHeaderView extends CalendarFXSample {

	private WeekDayHeaderView headerView;

	@Override
	public String getSampleName() {
		return "Week Day Header View";
	}

	@Override
	public Node getPanel(Stage stage) {
		StackPane outerPane = new StackPane();
		outerPane
				.setStyle("-fx-padding: 20px;");

		StackPane stackPane = new StackPane();
		stackPane
				.setStyle("-fx-background-color: white; -fx-border-color: gray; -fx-border-width: .25px; -fx-padding: 20px;");
		outerPane.getChildren().add(stackPane);

		headerView = new WeekDayHeaderView();
		headerView.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);

		StackPane.setAlignment(headerView, Pos.CENTER);

		stackPane.getChildren().add(headerView);

		return outerPane;
	}

	@Override
	public Node getControlPanel() {
		return new CalendarPropertySheet(headerView.getPropertySheetItems());
	}

	@Override
	public String getSampleDescription() {
		return "The week day header view displays the labels for each week day.";
	}

	public static void main(String[] args) {
		launch(args);
	}
}
