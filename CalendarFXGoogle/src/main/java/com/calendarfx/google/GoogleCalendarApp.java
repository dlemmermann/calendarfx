/**
 * Copyright (C) 2015, 2016 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.google;

import com.calendarfx.google.view.GoogleCalendarAppView;
import com.calendarfx.util.CalendarFX;
import com.calendarfx.util.LoggingDomain;
import com.calendarfx.view.CalendarView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.time.LocalTime;

public class GoogleCalendarApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        if (!CalendarFX.isLicenseKeySet()) {
            CalendarFX.setLicenseKey(
                    "LIC=DLSCDemos;VEN=DLSC;VER=1;PRO=STANDARD;RUN=no;CTR=1;SignCode=3F;Signature=302D0214047E13E511BF6104D2C543D02EA7EE789BF681CC02150084738BC8911A25ECBD081EA0ABD4E24D12BD5171");
        }

        LoggingDomain.CONFIG.info("Java version: " + System.getProperty("java.version"));

        CalendarView calendarView = new CalendarView();
        calendarView.setToday(LocalDate.now());
        calendarView.setTime(LocalTime.now());
        calendarView.setShowDeveloperConsole(Boolean.getBoolean("calendarfx.developer"));

        GoogleCalendarAppView appView = new GoogleCalendarAppView(calendarView);
        appView.getStylesheets().add(CalendarView.class.getResource("calendar.css").toExternalForm());

        primaryStage.setTitle("Google Calendar");
        primaryStage.setScene(new Scene(appView));
        primaryStage.setWidth(1400);
        primaryStage.setHeight(950);
        primaryStage.centerOnScreen();
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
