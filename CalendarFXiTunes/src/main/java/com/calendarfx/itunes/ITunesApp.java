/**
 * Copyright (C) 2015, 2016 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.itunes;

import static java.lang.Thread.MIN_PRIORITY;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import com.calendarfx.itunes.model.ITunesCalendarSource;
import com.calendarfx.itunes.model.ITunesEntry;
import com.calendarfx.itunes.view.BlurPane;
import com.calendarfx.util.CalendarFX;
import com.calendarfx.view.CalendarView;

import javafx.application.Application;
import javafx.beans.Observable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class ITunesApp extends Application {

	private BlurPane blurPane;
	private CalendarView calendarView;

	@Override
	public void start(Stage primaryStage) throws Exception {

		/*
		 * Need to check this as we might be launching this app from another
		 * app, e.g. from the sampler app.
		 */
		if (!CalendarFX.isLicenseKeySet()) {
			CalendarFX
					.setLicenseKey("LIC=EGO;VEN=DLSC;VER=1;PRO=STANDARD;RUN=no;CTR=1;SignCode=3F;Signature=302D021500966F11B77AEF4535FCCA596D57D2BE1F154AB54002140120A53DABE544B41CAF55851FD2F43395B1FED9");
		}

		calendarView = new CalendarView();
		calendarView.setHeader(createHeader());
		calendarView.getCalendarSources().addAll(new ITunesCalendarSource());

		Thread updateTimeThread = new Thread("Calendar: Update Time Thread") {
			@Override
			public void run() {
				while (true) {
					calendarView.setToday(LocalDate.now());
					calendarView.setTime(LocalTime.now());
					try {
						// update every 10 seconds
						sleep(10000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
			}
		};

		updateTimeThread.setPriority(MIN_PRIORITY);
		updateTimeThread.setDaemon(true);
		updateTimeThread.start();

		blurPane = new BlurPane();
		blurPane.setVisible(false);

		calendarView.getSelections().addListener(
				(Observable it) -> showMedia());

		StackPane stackPane = new StackPane();
		stackPane.getChildren().add(calendarView);
		stackPane.getChildren().add(blurPane);

		Scene scene = new Scene(stackPane);
		scene.getStylesheets().add(
				ITunesApp.class.getResource("itunes-app.css").toExternalForm());
		primaryStage.setTitle("iTunes Release Calendar");
		primaryStage.setScene(scene);
		primaryStage.setWidth(1300);
		primaryStage.setHeight(1000);
		primaryStage.centerOnScreen();
		primaryStage.show();
	}

	private void showMedia() {
        ITunesEntry entry = (ITunesEntry) (new ArrayList<>(
                calendarView.getSelections())).get(0);
		String url = entry.getUrl();
		System.out.println(url);
		WebView webView = new WebView();
		webView.setMaxSize(1100, 600);
		webView.setEffect(new DropShadow());
		webView.getEngine().load(url);
		blurPane.getChildren().add(webView);
		blurPane.setVisible(true);
	}

	private void showMedia2() {
		ITunesEntry entry = (ITunesEntry) (new ArrayList<>(
				calendarView.getSelections())).get(0);
		String url = entry.getUrl();
		System.out.println(url);
		MediaPlayer player = new MediaPlayer(new Media(url));
		MediaView mediaView = new MediaView(player);
		mediaView.setPreserveRatio(true);
		blurPane.getChildren().add(mediaView);
		blurPane.getChildren().add(new Label("Test"));
		blurPane.setVisible(true);
		player.play();
	}

	private Node createHeader() {
		VBox box = new VBox();
		WebView view = new WebView();
		WebEngine engine = view.getEngine();
		engine.loadContent("<body bgcolor=000000><center><div id='ibb-widget-root'></div><script>(function(t,e,i,d){var o=t.getElementById(i),n=t.createElement(e);o.style.height=90;o.style.width=728;o.style.display='inline-block';n.id='ibb-widget',n.setAttribute('src',('https:'===t.location.protocol?'https://':'http://')+d),n.setAttribute('width','728'),n.setAttribute('height','90'),n.setAttribute('frameborder','0'),n.setAttribute('scrolling','no'),o.appendChild(n)})(document,'iframe','ibb-widget-root',\"banners.itunes.apple.com/banner.html?partnerId=&aId=&bt=genre&t=genre_matrix_black&ft=topmovies&st=movie&s=33&p=15&c=us&l=en-US&w=728&h=90\");</script>");
		view.setPrefSize(728, 120);
		box.getChildren().add(view);
		box.setFillWidth(true);
		return view;
	}

	public static void main(String[] args) {
		launch(args);
	}
}
