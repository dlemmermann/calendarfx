/**
 * Copyright (C) 2015, 2016 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.itunes.model;

import com.calendarfx.model.Calendar;
import com.calendarfx.model.Interval;

import javax.json.Json;
import javax.json.stream.JsonParser;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class ITunesCalendar extends Calendar {

	public enum Media {
		APPS, MUSIC, MOVIES, BOOKS
	}

	private Media media;

	private DateTimeFormatter format = DateTimeFormatter.ISO_DATE_TIME;

	private enum Value {
		NONE, TRACK, DATE, ADDRESS
	}

	public ITunesCalendar(Media media) {
		super();

		this.media = media;

		switch (media) {
		case APPS:
			setName("Apps");
			setStyle(Style.STYLE1);
			break;
		case BOOKS:
			setName("Books");
			setStyle(Style.STYLE2);
			break;
		case MOVIES:
			setName("Movies");
			setStyle(Style.STYLE3);
			break;
		case MUSIC:
			setName("Music");
			setStyle(Style.STYLE4);
			break;
		}

		loadEntries();
	}

	private void loadEntries() {
		try {
			URL url = getURL();

			JsonParser parser = Json.createParser(url.openStream());

			ITunesEntry entry = null;

			Value value = Value.NONE;

			String title = null;
			LocalDate date = null;
			String address = null;

			while (parser.hasNext()) {

				JsonParser.Event event = parser.next();
				switch (event) {
				case START_ARRAY:
				case END_ARRAY:
				case START_OBJECT:
				case END_OBJECT:
				case VALUE_FALSE:
				case VALUE_NULL:
				case VALUE_TRUE:
					// System.out.println(event.toString());
					break;
				case KEY_NAME:
					// System.out.print(event.toString() + " "
					// + parser.getString() + " - ");
					if (parser.getString().equals("trackName")
							|| parser.getString().equals("collectionName")) {
						value = Value.TRACK;
					} else if (parser.getString().equals("releaseDate")) {
						value = Value.DATE;
					} else if (parser.getString().equals("previewUrl")
							|| parser.getString().equals("collectionViewUrl")
							|| parser.getString().equals("trackViewUrl")) {
						value = Value.ADDRESS;
					} else {
						value = Value.NONE;
					}
					break;
				case VALUE_STRING:
				case VALUE_NUMBER:
					// System.out.println(event.toString() + " "
					// + parser.getString());

					switch (value) {
					case TRACK:
						title = parser.getString();
						break;
					case DATE:
						date = LocalDate.parse(parser.getString(), format);
						break;
					case ADDRESS:
						address = parser.getString();
						break;
					default:
						break;
					}
					break;
				}

				if (title != null && date != null && address != null) {
					entry = new ITunesEntry(this);
					entry.setTitle(title);
					entry.setInterval(new Interval(date, LocalTime.MIN, date, LocalTime.MAX));
					entry.setFullDay(true);
					entry.setUrl(address);
					entry.setCalendar(this);

					System.out.println("media = " + media + ", title = " + entry.getTitle() + ", url = " + entry.getUrl());

					title = null;
					date = null;
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public final Media getMedia() {
		return media;
	}

	private final URL getURL() throws MalformedURLException {
		String year = "2014";
		String country = "ch";
		String limit = "50";

		String mediaText = "";
		String entity = "";

		switch (media) {
		case MUSIC:
			mediaText = "music";
			entity = "album";
			break;
		case APPS:
			mediaText = "software";
			entity = "software";
			break;
		case BOOKS:
			mediaText = "ebook";
			entity = "ebook";
			break;
		case MOVIES:
			mediaText = "movie";
			entity = "movie";
			break;
		}

		String url = null;

		switch (media) {
		case MOVIES:
			url = MessageFormat
					.format("https://itunes.apple.com/search?term={0}&media={1}&entity={2}&attribute=releaseYearTerm&limit={3}&country={4}",
							year, mediaText, entity, limit, country);
			break;
		case MUSIC:
		case BOOKS:
		case APPS:
			url = MessageFormat
					.format("https://itunes.apple.com/search?term={0}&entity={1}&limit={2}&country={3}",
							year, entity, limit, country);
			break;
		}

		System.out.println("URL: " + url);

		return new URL(url);

	}
}
