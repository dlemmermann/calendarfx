/**
 * Copyright (C) 2015, 2016 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.itunes.model;

import com.calendarfx.itunes.model.ITunesCalendar.Media;
import com.calendarfx.model.CalendarSource;

public class ITunesCalendarSource extends CalendarSource {

	public ITunesCalendarSource() {
		super("Releases");

		for (Media media : Media.values()) {
			getCalendars().add(new ITunesCalendar(media));
		}
	}
}
