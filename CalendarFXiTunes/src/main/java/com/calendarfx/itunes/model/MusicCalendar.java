/**
 * Copyright (C) 2015, 2016 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.itunes.model;


public class MusicCalendar extends ITunesCalendar {

	public MusicCalendar() {
		super(Media.MUSIC);
	}
}
