/**
 * Copyright (C) 2015, 2016 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.itunes.model;

import static java.util.Objects.requireNonNull;

import com.calendarfx.itunes.model.ITunesCalendar.Media;
import com.calendarfx.model.Entry;

public class ITunesEntry extends Entry<Object> {

	private String url;
	private ITunesCalendar calendar;

	protected ITunesEntry(ITunesCalendar calendar) {
		super();
		this.calendar = requireNonNull(calendar);
		setFullDay(true);
	}

	public final Media getMedia() {
		return calendar.getMedia();
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	@Override
	public boolean matches(String searchTerm) {
		return getTitle().toLowerCase().contains(searchTerm.toLowerCase());
	}
}
