/**
 * Copyright (C) 2015, 2016 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.licensing.util;

import com.smardec.license4j.LicenseUtil;

/**
 * Creates key pair in the specified file.
 */
public class CreateKeyPair {
	public static void main(String[] args) {
		try {
			LicenseUtil.createKeyPair("generated_keys.properties");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}