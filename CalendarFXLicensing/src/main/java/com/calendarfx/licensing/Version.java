/**
 * Copyright (C) 2015, 2016 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.licensing;

public enum Version {
	VERSION_1("1"),
	VERSION_2("8");

	String text;

	Version(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}
}
