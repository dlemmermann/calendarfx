/**
 * Copyright (C) 2015, 2016 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.licensing;

import com.calendarfx.util.CalendarFX;
import com.smardec.license4j.License;
import com.smardec.license4j.LicenseManager;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.Properties;

public class LicensingHelper {

	public static void createVendorLicenseKeys(Version v) {
		createLicenses("ComponentSource", "CO", false, v);
		createLicenses("ComponentSource", "CO", true, v);
		createLicenses("Kapitec", "KA", false, v);
		createLicenses("Kapitec", "KA", true, v);
	}

	public static void initPrivateKey(Version version) throws IOException {
		Properties props = new Properties();
		props.load(LicensingHelper.class
				.getResourceAsStream("private_key_version" + version.getText()
						+ ".properties"));
		LicenseManager.setPrivateKey(props.getProperty("private"));
	}

	/**
	 * Creates license from scratch.
	 */
	public static void createLicenses(String vendor, String customerPrefix,
			boolean runtime, Version version) {
		File file = null;
		if (runtime) {
			file = new File("keys_runtime_" + vendor + "_version_"
					+ version.getText() + ".txt");
		} else {
			file = new File("keys_development_" + vendor + "_version_"
					+ version.getText() + ".txt");
		}
		try {
			initPrivateKey(version);
			FileWriter fileWriter = new FileWriter(file);
			for (int i = 0; i < 999; i++) {
				String license = createSingleLicense(customerPrefix + "_" + i,
						vendor, runtime, "STANDARD", version.getText(), 0);
				fileWriter.append(license);
				fileWriter.append(System.getProperty("line.separator"));
			}
			fileWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String createSingleLicense(String licensee, String vendor,
			boolean runtime, String version, String product, int count)
			throws IllegalArgumentException, GeneralSecurityException,
			IOException {

		License license = new License();
		license.addFeature(CalendarFX.FEATURE_LICENSEE, licensee);
		license.addFeature(CalendarFX.FEATURE_VENDOR, vendor);
		license.addFeature(CalendarFX.FEATURE_VERSION, version);
		license.addFeature(CalendarFX.FEATURE_PRODUCT, product);
		if (runtime) {
			license.addFeature(CalendarFX.FEATURE_RUNTIME, "yes");
		} else {
			license.addFeature(CalendarFX.FEATURE_RUNTIME, "no");
		}
		license.addFeature("CTR", Integer.toString(count));
		LicenseManager.setSerializeStrings(false);
		String fileName = licensee + ".lic";
		LicenseManager.saveLicense(license, fileName);
		File tmpFile = new File(fileName);
		FileReader reader = new FileReader(tmpFile);
		BufferedReader buffer = new BufferedReader(reader);
		String line = buffer.readLine();
		StringBuffer sb = new StringBuffer();
		while (line != null) {
			sb.append(line);
			line = buffer.readLine();
			if (line != null) {
				sb.append(";");
			}
		}
		buffer.close();
		tmpFile.delete();
		return sb.toString();
	}

	public static void main(String[] args) {
		createVendorLicenseKeys(Version.VERSION_1);
		createVendorLicenseKeys(Version.VERSION_2);
	}
}
