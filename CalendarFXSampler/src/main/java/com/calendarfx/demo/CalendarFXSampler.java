/**
 * Copyright (C) 2015, 2016 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.demo;

import com.calendarfx.util.CalendarFX;
import fxsampler.FXSampler;
import javafx.stage.Stage;

public class CalendarFXSampler extends FXSampler {

	@Override
	public void start(Stage primaryStage) throws Exception {
		super.start(primaryStage);
	}

	public static void main(String[] args) {
		CalendarFX.setLicenseKey("LIC=DLSC;VEN=DLSC;VER=1;PRO=STANDARD;RUN=no;CTR=1;SignCode=3F;Signature=302C02143F0E526DBA5A9B4BA017B9F73746C5ACDF8EC39602142ECAF86DEB378E510CC677ACA11B0E546B3D7B5A");
		FXSampler.main(args);
	}
}
