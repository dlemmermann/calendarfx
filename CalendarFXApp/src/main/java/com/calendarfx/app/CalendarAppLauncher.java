/**
 * Copyright (C) 2015, 2016 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.app;

import com.calendarfx.util.CalendarFX;

public class CalendarAppLauncher {

    public static void main(String[] args) {
        System.setProperty("calendarfx.developer", "true");
        CalendarFX.setLicenseKey("LIC=DLSC;VEN=DLSC;VER=1;PRO=STANDARD;RUN=no;CTR=1;SignCode=3F;Signature=302C02143F0E526DBA5A9B4BA017B9F73746C5ACDF8EC39602142ECAF86DEB378E510CC677ACA11B0E546B3D7B5A");
        CalendarApp.main(args);
    }
}
