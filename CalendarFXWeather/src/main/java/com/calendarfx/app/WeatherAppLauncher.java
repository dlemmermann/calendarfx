/**
 * Copyright (C) 2015, 2016 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.app;

import com.calendarfx.util.CalendarFX;

public class WeatherAppLauncher {

    public static void main(String[] args) {
        System.setProperty("calendarfx.developer", "true");
        CalendarFX.setLicenseKey("LIC=Test;VEN=DLSC;VER=2;PRO=STANDARD;RUN=no;CTR=1;SignCode=3F;Signature=302C02143E51E97E5818184A1120BF49479463B36E75A8FD021459B7BEEB42CA245A57F169FB1B4F0E11F9373615");
        WeatherApp.main(args);
    }
}
