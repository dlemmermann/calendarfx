<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.calendarfx</groupId>
	<artifactId>calendar</artifactId>
	<version>8.4.1</version>
	<packaging>pom</packaging>
	<name>CalendarFX</name>
	<description>The parent project for the various CalendarFX modules.</description>
	<organization>
		<name>Dirk Lemmermann Software &amp; Consulting</name>
	</organization>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<modules>
		<module>../CalendarFXRecurrence</module>
		<module>../CalendarFXSampler</module>
		<module>../CalendarFXView</module>
		<module>../CalendarFXExperimental</module>
		<module>../CalendarFXGoogle</module>
		<module>../CalendarFXApp</module>
		<module>../CalendarFXiTunes</module>
		<module>../CalendarFXiCal</module>
		<module>../CalendarFXLicensing</module>
		<module>../CalendarFXWeather</module>
		<module>../CalendarFXAssembly</module>
	</modules>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>

			<plugin>
				<groupId>com.google.code.maven-license-plugin</groupId>
				<artifactId>maven-license-plugin</artifactId>
				<configuration>
					<header>${project.basedir}/../CalendarFX/license-header.txt</header>
					<includes>
						<include>**/*.java</include>
					</includes>
					<excludes>
						<exclude>target/**</exclude>
						<exclude>m2-target/**</exclude>
						<exclude>**/*.properties</exclude>
					</excludes>
					<properties>
						<name>CalendarFX</name>
						<year>2015, 2016, 2017</year>
						<holder>Dirk Lemmermann Software &amp; Consulting (dlsc.com)</holder>
						<contact>dlemmermann@gmail.com</contact>
					</properties>
					<encoding>UTF-8</encoding>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>format</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

		</plugins>
	</build>

	<dependencies>
		<!-- Test Dependencies -->

		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.11</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.hamcrest</groupId>
			<artifactId>hamcrest-all</artifactId>
			<version>1.3</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-all</artifactId>
			<version>1.9.5</version>
			<scope>test</scope>
		</dependency>

	</dependencies>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>cobertura-maven-plugin</artifactId>
				<version>2.6</version>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>2.10.1</version>
 				<configuration>
					<windowtitle>CalendarFX API</windowtitle>
					<javadocExecutable>/Library/Java/JavaVirtualMachines/jdk1.8.0.jdk/Contents/Home/bin/javadoc</javadocExecutable>
					<aggregate>true</aggregate>
					<show>protected</show>
					<failOnError>false</failOnError>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<version>2.11</version>
				<reportSets>
					<reportSet>
						<reports>
							<report>checkstyle</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
		</plugins>
	</reporting>

	<profiles>
		<profile>
			<id>local</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-javadoc-plugin</artifactId>
						<version>2.10.1</version>
						<configuration>
							<windowtitle>CalendarFX API</windowtitle>
							<javadocExecutable>/Library/Java/JavaVirtualMachines/jdk1.8.0_121.jdk/Contents/Home/bin/javadoc</javadocExecutable>
							<additionalparam>--allow-script-in-comments</additionalparam>
							<aggregate>true</aggregate>
							<detectLinks>true</detectLinks>
							<links>
								<link>http://docs.oracle.com/javase/8/docs/api/</link>
								<link>http://docs.oracle.com/javase/8/javafx/api/</link>
								<link>http://controlsfx.bitbucket.org</link>
							</links>
							<additionalJOption>-J-Djavafx.javadoc=true</additionalJOption>
							<groups>
								<group>
									<title>Model</title>
									<packages>com.calendarfx.model*</packages>
								</group>
								<group>
									<title>View</title>
									<packages>com.calendarfx.view*</packages>
								</group>
								<group>
									<title>Util</title>
									<packages>com.calendarfx.util*</packages>
								</group>
							</groups>
							<excludePackageNames>com.google.*:com.calendarfx.experimental:impl.com.calendarfx:com.calendarfx.licensing:com.calendarfx.demo:com.calendarfx.app:com.calendarfx.google:com.calendarfx.ical:com.calendarfx.itunes:com.lynden</excludePackageNames>
							<failOnError>false</failOnError>
							<docfilessubdirs>true</docfilessubdirs>

							<header>
					<![CDATA[
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1658026-2', 'auto');
  ga('send', 'pageview');

</script>						 ]]>
							</header>
						</configuration>
						<executions>
							<execution>
								<id>make-docs</id> <!-- this is used for inheritance merges -->
								<phase>package</phase> <!-- bind to the packaging phase -->
								<goals>
									<goal>aggregate</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>

		<profile>
			<id>server</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-javadoc-plugin</artifactId>
						<version>2.10.1</version>
						<configuration>
							<windowtitle>CalendarFX API</windowtitle>
							<detectLinks>true</detectLinks>
							<links>
								<link>http://docs.oracle.com/javase/8/docs/api/</link>
								<link>http://docs.oracle.com/javase/8/javafx/api/</link>
							</links>
							<additionalJOption>-J-Djavafx.javadoc=true</additionalJOption>
							<detectJavaApiLink>true</detectJavaApiLink>
							<groups>
								<group>
									<title>Model</title>
									<packages>com.calendarfx.model*</packages>
								</group>
								<group>
									<title>View</title>
									<packages>com.calendarfx.view*</packages>
								</group>
								<group>
									<title>Util</title>
									<packages>com.calendarfx.util*</packages>
								</group>
							</groups>
							<excludePackageNames>com.google.*:com.calendarfx.experimental:impl.com.calendarfx:com.calendarfx.licensing:com.calendarfx.demo:com.calendarfx.app:com.calendarfx.google:com.calendarfx.ical:com.calendarfx.itunes:com.lynden</excludePackageNames>
							<failOnError>false</failOnError>
							<docfilessubdirs>true</docfilessubdirs>
						</configuration>
						<executions>
							<execution>
								<id>make-docs</id> <!-- this is used for inheritance merges -->
								<phase>package</phase> <!-- bind to the packaging phase -->
								<goals>
									<goal>aggregate</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>

	</profiles>

</project>