/**
 * Copyright (C) 2015, 2016 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package com.calendarfx.util;

import com.smardec.license4j.License;
import com.smardec.license4j.LicenseManager;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Common superclass for all controls in this framework.
 */
public final class CalendarFX {

	private static String version;

	/**
	 * Returns the CalendarFX version number in the format major.minor.bug
	 * (1.0.0).
	 *
	 * @return the CalendarFX version number
	 */
	public static String getVersion() {
		if (version == null) {
			InputStream stream = CalendarFX.class
					.getResourceAsStream("version.properties"); //$NON-NLS-1$
			Properties props = new Properties();
			try {
				props.load(stream);
			} catch (IOException ex) {
				LoggingDomain.CONFIG.throwing(CalendarFX.class.getName(),
						"getVersion()", ex); //$NON-NLS-1$
			}
			version = props.getProperty("calendarfx.version", "1.0.0"); //$NON-NLS-1$ //$NON-NLS-2$

			LoggingDomain.CONFIG.info("CalendarFX Version: " + version); //$NON-NLS-1$
		}
		return version;
	}

	/**
	 * Represents the "Licensee" feature.
	 */
	public static final String FEATURE_LICENSEE = "LIC"; //$NON-NLS-1$

	/**
	 * Represents the "Product" feature.
	 */
	public static final String FEATURE_PRODUCT = "PRO"; //$NON-NLS-1$

	/**
	 * Represents the "Runtime" feature.
	 */
	public static final String FEATURE_RUNTIME = "RUN"; //$NON-NLS-1$

	/**
	 * Represents the "Vendor" feature.
	 */
	public static final String FEATURE_VENDOR = "VEN"; //$NON-NLS-1$

	/**
	 * Represents the "Version" feature.
	 */
	public static final String FEATURE_VERSION = "VER"; //$NON-NLS-1$

	/*
	 * A flag that gets used to check whether a license key was set more than
	 * once.
	 */
	private static boolean keySet;

	/*
	 * Stores the license4j license object.
	 */
	private static License license;

	/*
	 * Standard logger instance.
	 */
	private static final Logger LOGGER = Logger.getLogger(CalendarFX.class
			.getName());

	/*
	 * Stores the public key needed by license4j.
	 */
	private static String publicKey;

	/*
	 * The system exit code that will be used if the license key validation
	 * fails.
	 */
	private static final int SYSTEM_EXIT_CODE = -1603;

	static {
		Properties props = new Properties();
		try {
			props.load(CalendarFX.class
					.getResourceAsStream("public_key.properties")); //$NON-NLS-1$
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE,
					"unable to process public key property file", e); //$NON-NLS-1$
		}
		publicKey = props.getProperty("public"); //$NON-NLS-1$
		LicenseManager.setPublicKey(publicKey);
	}

	/**
	 * Returns the name of the licensee.
	 *
	 * @return the licensee's name
	 */
	public static String getLicensee() {
		if (getLicense() != null) {
			return (String) getLicense().getFeature(FEATURE_LICENSEE);
		}
		return "---"; //$NON-NLS-1$
	}

	/**
	 * Returns the product type (e.g. "LITE", "STANDARD", "ENTERPRISE").
	 *
	 * @return the product type
	 */
	public static String getProduct() {
		if (getLicense() != null) {
			return (String) getLicense().getFeature(FEATURE_PRODUCT);
		}
		return "---"; //$NON-NLS-1$
	}

	/**
	 * Returns the public key used for decoding the license key.
	 *
	 * @return the public key used for decoding the license key
	 */
	public static String getPublicKey() {
		return publicKey;
	}

	/**
	 * Returns the name of the vendor (e.g. ComponentSource, DLSC, Evget, ...).
	 *
	 * @return the vendor's name
	 */
	public static String getVendor() {
		if (getLicense() != null) {
			return (String) getLicense().getFeature(FEATURE_VENDOR);
		}
		return "---"; //$NON-NLS-1$
	}

	/**
	 * Returns the version (e.g. "1", "2", ...). This number is the version
	 * number used for licensing issues only. It represents the major version
	 * number. A more detailed version number can be looked up by calling
	 * {@link CalendarFX#getVersion()}.
	 *
	 * @return the product version
	 */
	public static String getLicensedVersion() {
		if (getLicense() != null) {
			return (String) getLicense().getFeature(FEATURE_VERSION);
		}
		return "---"; //$NON-NLS-1$
	}

	/**
	 * Determines if the product uses a development license.
	 *
	 * @return true if the product uses a development license
	 */
	public static boolean isDevelopmentLicense() {
		return !isTrialLicense() && !isRuntimeLicense();
	}

	/**
	 * Determines if the product uses a runtime license.
	 *
	 * @return true if the product uses a runtime license
	 */
	public static boolean isRuntimeLicense() {
		if (getLicense() != null) {
			String runtime = (String) getLicense().getFeature(FEATURE_RUNTIME);
			return runtime != null && runtime.equals("yes"); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Determines if the product is run as a trial.
	 *
	 * @return true if the product is run as a trial
	 */
	public static boolean isTrialLicense() {
		return getLicense() == null;
	}

	/**
	 * Determines if the {@link #setLicenseKey(String)} method has been called.
	 *
	 * @return true if the license key has already been set
	 */
	public static boolean isLicenseKeySet() {
		return keySet;
	}

	/**
	 * Sets the license key used for FlexGantt. The key determines whether the
	 * product uses a development or a runtime license.
	 *
	 * @param key
	 *            the license key
	 * @throws IllegalStateException
	 *             if the license key gets set more than once
	 */
	public static void setLicenseKey(String key) {
		if (keySet) {
			throw new IllegalStateException(
					"licensing key can only be set once"); //$NON-NLS-1$
		}
		keySet = true;
		LOGGER.fine("found properties file"); //$NON-NLS-1$
		try {
			key = key.replace(';', '\n');
			ByteArrayInputStream stream = new ByteArrayInputStream(
					key.getBytes());
			license = LicenseManager.loadLicense(stream);
			if (!LicenseManager.isValid(license)) {
				System.err.println();
				System.err.println("#########################################"); //$NON-NLS-1$
				System.err.println("# Invalid CalendarFX license key!       #"); //$NON-NLS-1$
				System.err.println("# Exiting application...                #"); //$NON-NLS-1$
				System.err.println("#########################################"); //$NON-NLS-1$
				System.err.println();
				System.exit(SYSTEM_EXIT_CODE);
			}
			@SuppressWarnings("unchecked")
			List<String> featureNames = license.getFeatureList();
			LOGGER.fine("License Features:"); //$NON-NLS-1$
			for (String featureName : featureNames) {
				Object featureValue = license.getFeature(featureName);
				LOGGER.fine("   " + featureName + " = " + featureValue); //$NON-NLS-1$ //$NON-NLS-2$
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE,
					"unable to process CalendarFX properties file", e); //$NON-NLS-1$
			System.err
					.println("Unable to process CalendarFX license key, exiting application"); //$NON-NLS-1$
			System.exit(SYSTEM_EXIT_CODE);
		}
	}

	/**
	 * Returns the license object.
	 *
	 * @return the license
	 */
	public static License getLicense() {
		if (license != null) {
			return license;
		}

		String key = System.getProperty("calendarfx.license"); //$NON-NLS-1$
		if (key != null) {
			setLicenseKey(key);
		}

		return license;
	}
}
