/**
 * Copyright (C) 2015, 2016 Dirk Lemmermann Software & Consulting (dlsc.com) 
 * 
 * This file is part of CalendarFX.
 */

package impl.com.calendarfx.view;

import com.calendarfx.model.Calendar;
import com.calendarfx.model.CalendarEvent;
import com.calendarfx.model.Entry;
import com.calendarfx.model.Interval;
import com.calendarfx.util.CalendarFX;
import com.calendarfx.util.LoggingDomain;
import com.calendarfx.view.DateControl;
import com.calendarfx.view.DraggedEntry;
import impl.com.calendarfx.view.util.Util;
import javafx.beans.InvalidationListener;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.MapChangeListener;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.SkinBase;

import java.time.*;
import java.util.logging.Level;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import static com.calendarfx.model.CalendarEvent.CALENDAR_CHANGED;
import static com.calendarfx.util.LoggingDomain.CONFIG;
import static javafx.scene.input.MouseEvent.MOUSE_PRESSED;

public abstract class DateControlSkin<C extends DateControl> extends SkinBase<C> {

    public DateControlSkin(C control) {
        super(control);

        control.addEventHandler(MOUSE_PRESSED, evt -> control.clearSelection());

        control.getCalendars().addListener(this::calendarListChanged);

        for (Calendar calendar : control.getCalendars()) {
            calendar.addEventHandler(calendarListener);
        }

        MapChangeListener<? super Object, ? super Object> propertiesListener = change -> {
            if (change.wasAdded()) {
                if (change.getKey().equals("refresh.data")) { //$NON-NLS-1$
                    LoggingDomain.VIEW.fine("data refresh was requested by the application"); //$NON-NLS-1$
                    control.getProperties().remove("refresh.data"); //$NON-NLS-1$
                    refreshData();
                }
            }
        };

        control.getProperties().addListener(propertiesListener);

        InvalidationListener calendarVisibilityListener = it -> calendarVisibilityChanged();

        for (Calendar calendar : control.getCalendars()) {
            control.getCalendarVisibilityProperty(calendar).addListener(calendarVisibilityListener);
        }

        control.getCalendars().addListener((Change<? extends Calendar> change) -> {
            while (change.next()) {
                if (change.wasAdded()) {
                    for (Calendar calendar : change.getAddedSubList()) {
                        control.getCalendarVisibilityProperty(calendar).addListener(calendarVisibilityListener);
                    }
                } else if (change.wasRemoved()) {
                    for (Calendar calendar : change.getRemoved()) {
                        control.getCalendarVisibilityProperty(calendar).removeListener(calendarVisibilityListener);
                    }
                }
            }
        });

        nagging();
    }

    protected void refreshData() {
    }

    private InvalidationListener calendarVisibilityChanged = it -> calendarVisibilityChanged();

    protected void calendarVisibilityChanged() {
    }

    private EventHandler<CalendarEvent> calendarListener = this::calendarChanged;

    private void calendarListChanged(Change<? extends Calendar> change) {
        C dateControl = getSkinnable();
        while (change.next()) {
            if (change.wasAdded()) {
                for (Calendar calendar : change.getAddedSubList()) {
                    calendar.addEventHandler(calendarListener);
                    dateControl.getCalendarVisibilityProperty(calendar).addListener(calendarVisibilityChanged);
                }
            } else if (change.wasRemoved()) {
                for (Calendar calendar : change.getRemoved()) {
                    calendar.removeEventHandler(calendarListener);
                    dateControl.getCalendarVisibilityProperty(calendar).removeListener(calendarVisibilityChanged);
                }
            }
        }
    }

    private void calendarChanged(CalendarEvent evt) {

        if (LoggingDomain.EVENTS.isLoggable(Level.FINE) && !(evt.getEntry() instanceof DraggedEntry)) {
            LoggingDomain.EVENTS.fine("calendar event in " + getSkinnable().getClass().getSimpleName() + ": " + evt.getEventType() + ", details: " + evt.toString());
        }

        if (getSkinnable().isSuspendUpdates()) {
            return;
        }

        if (evt.getEventType().getSuperType().equals(CalendarEvent.ENTRY_CHANGED) && evt.getEntry().isRecurrence()) {
            return;
        }

        Util.runInFXThread(() -> {
            EventType<? extends Event> eventType = evt.getEventType();
            if (eventType.equals(CalendarEvent.ENTRY_INTERVAL_CHANGED)) {
                entryIntervalChanged(evt);
            } else if (eventType.equals(CalendarEvent.ENTRY_FULL_DAY_CHANGED)) {
                entryFullDayChanged(evt);
            } else if (eventType.equals(CalendarEvent.ENTRY_RECURRENCE_RULE_CHANGED)) {
                entryRecurrenceRuleChanged(evt);
            } else if (eventType.equals(CalendarEvent.ENTRY_CALENDAR_CHANGED)) {
                entryCalendarChanged(evt);
            } else if (eventType.equals(CALENDAR_CHANGED)) {
                calendarChanged(evt.getCalendar());
            }
        });
    }

    protected void entryIntervalChanged(CalendarEvent evt) {
    }

    protected void entryFullDayChanged(CalendarEvent evt) {
    }

    protected void entryRecurrenceRuleChanged(CalendarEvent evt) {
    }

    protected void entryCalendarChanged(CalendarEvent evt) {
    }

    protected void calendarChanged(Calendar calendar) {
    }

    protected boolean isRelevant(Entry<?> entry) {

        if (this instanceof LoadDataSettingsProvider) {
            C dateControl = getSkinnable();

            if (!(entry instanceof DraggedEntry) && !dateControl.isCalendarVisible(entry.getCalendar())) {
                return false;
            }

            LoadDataSettingsProvider provider = (LoadDataSettingsProvider) this;

            ZoneId zoneId = getSkinnable().getZoneId();
            LocalDate loadStartDate = provider.getLoadStartDate();
            LocalDate loadEndDate = provider.getLoadEndDate();

            return entry.isShowing(loadStartDate, loadEndDate, zoneId);
        }

        return false;
    }

    protected boolean isRelevant(Interval interval) {
        LoadDataSettingsProvider provider = (LoadDataSettingsProvider) this;
        ZoneId zoneId = getSkinnable().getZoneId();
        LocalDate loadStartDate = provider.getLoadStartDate();
        LocalDate loadEndDate = provider.getLoadEndDate();
        ZonedDateTime st = ZonedDateTime.of(loadStartDate, LocalTime.MIN, zoneId);
        ZonedDateTime et = ZonedDateTime.of(loadEndDate, LocalTime.MAX, zoneId);
        return Util.intersect(interval.getStartZonedDateTime(), interval.getEndZonedDateTime(), st, et);
    }

    /*
     * Used as a key for the preference, which stores the date when the nagging
     * screen was last shown.
     */
    private static final String NAGGING_DATE = "NAG_DATE"; //$NON-NLS-1$

    /*
     * Used as a key for the preference, which stores the date when the code was
     * run for the first time.
     */
    private static final String INSTALLATION_DATE = "INST_DATE"; //$NON-NLS-1$

    private static boolean naggingShown;

    private synchronized void nagging() {
        if (!naggingShown && !CalendarFX.isRuntimeLicense()) {
            naggingShown = true;
            System.out.println("CalendarFX user interface framework for Java, (Version " + CalendarFX.getVersion() + ")"); //$NON-NLS-1$
            if (CalendarFX.isTrialLicense()) {
                System.out.println("Unlicensed evaluation / trial version."); //$NON-NLS-1$

				/*
                 * Show a nagging dialog, but only once every four hours. The
				 * dialog will cause a delay of several seconds.
				 */
                showNaggingNotification();

				/*
                 * First check whether the user is still within the 60 days
				 * evaluation period. If not cause a system exit.
				 */
                checkEvaluationPeriod();

				/*
                 * Cause an automatic system exit after thirty minutes.
				 */
                exitAfter30Minutes();

                System.out.println("Application will automatically shut down in 30 minutes"); //$NON-NLS-1$

            } else if (CalendarFX.isDevelopmentLicense()) {
                System.out.println("Development License (do not use in production systems)"); //$NON-NLS-1$
            }

            System.out.println("(c) 2016 Dirk Lemmermann Software & Consulting"); //$NON-NLS-1$
            System.out.println("http://www.calendarfx.com"); //$NON-NLS-1$
        }
    }

    /*
     * Show a nagging screen, but only once per day.
     */
    private void showNaggingNotification() {
        Preferences prefs = Preferences.userNodeForPackage(DateControlSkin.class);
        long systemTime = Instant.now().toEpochMilli();

        long naggingScreenLastShownOn = prefs.getLong(NAGGING_DATE, -1);
        if (naggingScreenLastShownOn == -1) {
            naggingScreenLastShownOn = systemTime;
            prefs.putLong(NAGGING_DATE, naggingScreenLastShownOn);
            try {
                prefs.flush();
            } catch (BackingStoreException ex) {
                CONFIG.throwing(DateControlSkin.class.getName(), "showNaggingScreen()", ex); //$NON-NLS-1$
            }
        }
        long duration = systemTime - naggingScreenLastShownOn;

        // only if 4 hours have passed since last time
        if (duration > (4 * 60 * 60 * 1000)) {
            prefs.putLong(NAGGING_DATE, systemTime);
            try {
                prefs.flush();
            } catch (BackingStoreException ex) {
                CONFIG.throwing(DateControlSkin.class.getName(), "showNaggingScreen()", ex); //$NON-NLS-1$
            }

            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("CalendarFX License"); //$NON-NLS-1$
            alert.setHeaderText("License check has failed."); //$NON-NLS-1$
            alert.setContentText("This is a trial version of CalendarFX. Do not use in production systems!\n\nFor more information about licensing options, please visit http://www.calendarfx.com"); //$NON-NLS-1$
            alert.show();
        }
    }

    /*
     * Cause a system exit after 30 minutes.
     */
    private void exitAfter30Minutes() {
        Thread thread = new Thread(() -> {
            try {
                Thread.sleep(30 * 60 * 1000); // 30 minutes
                System.out.println("### Trial duration expired after 30 minutes."); //$NON-NLS-1$
                System.out.println("### Exiting application."); //$NON-NLS-1$
                System.exit(0);
            } catch (InterruptedException e) {
                LoggingDomain.CONFIG.throwing(DateControlSkin.class.getName(), "run()", e); //$NON-NLS-1$
            }
        });
        thread.setName("LicenseThread"); //$NON-NLS-1$
        thread.setDaemon(true);
        thread.start();
    }

    /*
     * Check whether the user is still within the 30 day evaluation period.
     */
    private void checkEvaluationPeriod() {
        long remainingDays = getRemainingDays();
        if (remainingDays > 0) {
            System.out.println("Evaluation period ends in " + remainingDays + " days!"); //$NON-NLS-1$ //$NON-NLS-2$
        } else {
            System.out.println("#################################################################"); //$NON-NLS-1$
            System.out.println("#                                                               #"); //$NON-NLS-1$
            System.out.println("#   CalendarFX evaluation period has ended after 90 days!       #"); //$NON-NLS-1$
            System.out.println("#                                                               #"); //$NON-NLS-1$
            System.out.println("#################################################################"); //$NON-NLS-1$
            System.exit(0);
        }
    }

    private long getRemainingDays() {
        long systemTime = Instant.now().toEpochMilli();
        Preferences prefs = Preferences.userNodeForPackage(DateControlSkin.class);
        long instDate = prefs.getLong(INSTALLATION_DATE, -1);
        if (instDate == -1) {
            prefs.putLong(INSTALLATION_DATE, systemTime);
            try {
                prefs.flush();
            } catch (BackingStoreException ex) {
                CONFIG.throwing(DateControlSkin.class.getName(), "checkEvaluationPeriod()", ex); //$NON-NLS-1$
            }
            instDate = systemTime;
        }

        long daysUsed = (systemTime - instDate) / (24 * 60 * 60 * 1000);
        return 90 - daysUsed;
    }
}
